<?php
    
    session_start();

    $erreur_message = [];
    $temps_convertie_message  = '';
    $error = false;

    function convertisseur_temps($nombre , $b_convert , $u_convert){
        $temps = array(
            "sec"  => 1, 
            "min"  => 60, 
            "heure"  => 60*60,
            "jour"  => 60*60*24
        ); 
        return ($temps[$b_convert] / $temps[$u_convert] * $nombre);
    }

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if(!is_numeric($_POST['nombre_temps'])) {
        $error = true;
        $erreur_message[] ='  <div class="alert warning-alert">
                        <h3>Aie, il faut saisir une valeur valide</h3>
                        <a class="close">&times;</a>
                    </div>';

    } 

    if(empty($_POST['b_convert'])) {
        $error = true;
        $erreur_message[] ='  <div class="alert danger-alert">
                        <h3>Veillez choisir une unité de base</h3>
                        <a class="close">&times;</a>
                    </div>';
    }

    if(empty($_POST['c_convert'])) {
        $error = true;
        $erreur_message[] = '  <div class="alert danger-alert">
                        <h3>Veillez choisir une unité de convertion</h3>
                        <a class="close">&times;</a>
                    </div>';
    } 

    if (!$error) {

        $temps_convertie = round(convertisseur_temps($_POST['nombre_temps'], $_POST['b_convert'], $_POST['c_convert']), 2);
        $temps_convertie_message = '<span class="contact100-form-title" style="text-align: center; margin-top: 35px;">'. $_POST['nombre_temps'] . $_POST['b_convert'] . " en " . $_POST['c_convert'] . " donne " . $temps_convertie  . $_POST['c_convert']  . '</span>';
        $_SESSION['historique'][] = [$_POST['nombre_temps'], $_POST['b_convert'], $_POST['c_convert'], $temps_convertie];
    }
}
?>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CONVER TEMPS</title>

    <link rel="stylesheet" type="text/css" href="css/master.css?<?php echo rand(0,1000); ?>">
</head>
<body>
    
<div class="bg-contact100" style="background-image: url('images/bg-01.jpg');">
		<div class="container-contact100">
			<div class="wrap-contact100">
				<div class="contact100-pic js-tilt">
                    <section>
                        <h1>Historique</h1>
                        <div class="tbl-header">
                            <table cellpadding="0" cellspacing="0" border="0">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Unnité de base</th>
                                    <th>Unité de convertion</th>
                                    <th>Resultat</th>
                                </tr>
                            </thead>
                            </table>
                        </div>
                        <div class="tbl-content">
                            <table cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <?php
                                if (isset($_SESSION['historique'])) {
                                    for ($i=0; $i < count($_SESSION['historique']) ; $i++) { 
                                            echo "<tr>
                                                <td>" . $_SESSION['historique'][$i][0] . "</td>
                                                <td>" . $_SESSION['historique'][$i][1] . "</td>
                                                <td>" . $_SESSION['historique'][$i][2] . "</td>
                                                <td>" . $_SESSION['historique'][$i][3] . "</td>
                                            </tr>";
                                    }
                                }
                                ?>
                            </tbody>
                            </table>
                        </div>
                        	<button class="contact100-form-btn" onclick="window.location.href='./destroy.php'">
							    VIDER
						    </button>
                        </section>
                <?php
                   // print_r($_SESSION['historique']);

                ?>
				</div>

				<form class="contact100-form validate-form"  method="POST">
					<span class="contact100-form-title">
						CONVERTISSION DE DURÉE
					</span>

					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="nombre_temps" placeholder="Nombre" value="<?= $_POST['nombre_temps'] ?? ""; ?>">
						<span class="focus-input100"></span>
					</div>
                    <div class="wrap-input100 validate-input">
                        <select name="b_convert" class="input100" value="<?= $_POST['nombre_temps'] ?? ""; ?>">
                            <option value="">--Veillez choisir votre unité à convertir--</option>
                            <option value="sec">Secondes</option>
                            <option value="min">Minutes</option>
                            <option value="heure">Heure</option>
                            <option value="jour">Jour</option>
                        </select>
					</div>

                    <div class="wrap-input100 validate-input">
                        <select name="c_convert" class="input100">
                            <option value="">--Veillez choisir votre unité en laquelle vous voulez convertir--</option>
                            <option value="sec">Seconde</option>
                            <option value="min">Minute</option>
                            <option value="heure">Heure</option>
                            <option value="jour">Jour</option>
                        </select>
					</div>

					<div class="container-contact100-form-btn">
						<button class="contact100-form-btn">
							CONVERTIR
						</button>
					</div>
                <?php 
                    if($error) {
                        foreach($erreur_message as $msg) {
                            echo $msg;
                        }
                    } else {
                        echo $temps_convertie_message;
                    }
                ?>
				</form>
			</div>
		</div>
</div>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
<script> 
$(".close").click(function() {
  $(this)
    .parent(".alert")
    .fadeOut();
});
</script>


</body>
</html>
